[require]
GL >= 4.3
GLSL >= 4.30
GL_ARB_gpu_shader_int64
GL_ARB_shader_clock

[compute shader]
#version 430
#extension GL_ARB_gpu_shader_int64: require
#extension GL_ARB_shader_clock: require

layout(local_size_x = 1) in;

layout(binding = 0) uniform atomic_uint good;
layout(binding = 0) uniform atomic_uint bad;

layout(std430, binding = 0) buffer ssbo_data {
	uint64_t ssbo_time[];
};

void main() {
	uint64_t start_time = clockARB();

	ssbo_time[gl_WorkGroupID.x] = start_time;

	int64_t diff = int64_t(clockARB() - start_time);
	if (diff <= 0l)
		atomicCounterIncrement(bad);
	else
		atomicCounterIncrement(good);
}

[test]
atomic counters 2
ssbo 0 64

compute 8 1 1

probe atomic counter 0 == 8
probe atomic counter 1 == 0
